﻿using System.Threading.Tasks;

namespace Tream.Common.CQRS.Contracts
{
    public interface IQueryHandlerAsync<in TQuery, TResult>
        where TQuery: IQuery<TResult> 
    {
        Task<TResult> Handle(TQuery query);
    }
}
