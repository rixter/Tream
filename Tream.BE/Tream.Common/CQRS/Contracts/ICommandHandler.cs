﻿using System.Threading.Tasks;

namespace Tream.Common.CQRS.Contracts
{
    /// <summary>
    ///     Defines the command handler that changes the resource state
    /// </summary>
    /// <typeparam name="TCommand"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public interface ICommandHandler<in TCommand, TResult> where TCommand : ICommand
    {
        /// <summary>
        ///     Executes the command handling logic
        /// </summary>
        /// <param name="command"> Command instance. </param>
        /// <returns> Returns if execution completed without errors. </returns>
        Task<TResult> Handle(TCommand command);
    }
}
