﻿using System.Threading.Tasks;

namespace Tream.Common.CQRS.Contracts
{
    public interface ICommandProcessorAsync<TResult>
    {
        /// <summary>
        /// Process command that implements <see cref="ICommand" />.
        /// </summary>
        /// <param name="command">Command that should be processed.</param>
        Task<TResult> ProcessAsync(ICommand command);
    }
}
