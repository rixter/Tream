﻿using System.Threading.Tasks;

namespace Tream.Common.CQRS.Contracts
{
    public interface IQueryProcessorAsync
    {
        Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query);
    }
}
