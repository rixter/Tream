﻿namespace Tream.Common.CQRS.Contracts
{
    public interface IQuery<TResult>: IQuery
    {
    }

    /// <summary>
    /// Defines an query object
    /// </summary>
    public interface IQuery
    {
    }
}
