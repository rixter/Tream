﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tream.Common.CQRS
{
    public class CommandResult
    {
        public bool Success { get; }

        public string ErrorMessage { get; }

        /// <summary>
        ///     Creates command result instance with specified status
        /// </summary>
        /// <param name="success">Indicates wheter command has been completed successfully</param>
        public CommandResult(bool success)
        {
            Success = success;
        }
    }
}
