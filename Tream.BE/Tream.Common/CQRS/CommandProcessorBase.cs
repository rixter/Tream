﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tream.Common.CQRS.Contracts;

namespace Tream.Common.CQRS
{
    public class CommandProcessorBase<TResult> : ICommandProcessorAsync<TResult>
    {
        private readonly Dictionary<Type, Func<ICommand, Task<TResult>>> _handlerLocator = new Dictionary<Type, Func<ICommand, Task<TResult>>>();

        /// <inheritdoc />
        public Task<TResult> ProcessAsync(ICommand command)
        {
            return _handlerLocator[command.GetType()].Invoke(command);
        }
        /// <summary>
        /// Registers command handlers in dictionary.
        /// </summary>
        /// <typeparam name="TCommand">Represents command that implements <see cref="ICommand"/>.</typeparam>
        /// <typeparam name="THandler">Represents command handler that implements <see cref="ICommandHandler{TCommand, TResult}"/>.</typeparam>
        /// <param name="handler">Represents handler.</param>
        protected void RegisterHandler<TCommand, THandler>(THandler handler)
            where TCommand : class, ICommand
            where THandler : ICommandHandler<TCommand, TResult>
        {
            _handlerLocator[typeof(TCommand)] = command => handler.Handle(command as TCommand);
        }
    }
}
