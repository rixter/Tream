﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tream.Common.CQRS.Contracts;

namespace Tream.Common.CQRS
{
    public class QueryProcessorBase : IQueryProcessorAsync
    {
        private readonly Dictionary<Type, Func<IQuery, object>> _handlerLocator = new Dictionary<Type, Func<IQuery, object>>();
        
        /// <inheritdoc />
        public async Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query)
        {
            return await (Task<TResult>)_handlerLocator[query.GetType()].Invoke(query);
        }

        /// <summary>
        /// Registers queries handlers in dictionary.
        /// </summary>
        /// <typeparam name="TQuery">Represents query that implements <see cref="IQuery{TResult}"/>.</typeparam>
        /// <typeparam name="TResult">Represents result of query processing.</typeparam>
        /// <typeparam name="THandler">Represents query handler that implements <see cref="IQueryHandlerAsync{TQuery,TResult}"/>.</typeparam>
        /// <param name="handler">Represents handler.</param>
        protected void RegisterAsyncHandler<TQuery, TResult, THandler>(THandler handler)
            where TQuery : class, IQuery<TResult>
            where THandler : IQueryHandlerAsync<TQuery, TResult>
        {
            _handlerLocator[typeof(TQuery)] = query => handler.Handle(query as TQuery);
        }
    }
}
