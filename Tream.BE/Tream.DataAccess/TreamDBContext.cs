﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Tream.DataAccess.Models;

namespace Tream.DataAccess
{
    public class TreamDBContext: IdentityDbContext<User>
    {
        public DbSet<Place> Places { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<ProductOrder> ProductOrders { get; set; }

        public DbSet<ProductCategory> ProductCategories { get; set; }

        public TreamDBContext(DbContextOptions<TreamDBContext> options)
             : base(options)
        {
        }
    }
}
