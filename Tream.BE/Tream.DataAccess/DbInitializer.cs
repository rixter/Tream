﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tream.DataAccess.Models;

namespace Tream.DataAccess
{
    public static class DbInitializer
    {
        public static void Initialize(TreamDBContext context)
        {

            if (context.Users.Any())
            {
                return;
            }

            var users = new List<User>
            {
                new User
                {
                    Email = "user1@gmail.com",
                    UserName ="user1",
                    PasswordHash = "AQAAAAEAACcQAAAAEAdT9ZBXwB0cThZEsLfrGmKXcU+Ibn7sbO+dQhcQVgQKwGOlvhuty2SFfPSjGA3edQ=="
                },
                new User
                {
                    Email = "user@gmail.com",
                    UserName ="user",
                    PasswordHash = "AQAAAAEAACcQAAAAEAdT9ZBXwB0cThZEsLfrGmKXcU+Ibn7sbO+dQhcQVgQKwGOlvhuty2SFfPSjGA3edQ=="
                },
            };

            context.Users.AddRange(users);
            context.SaveChanges();

            var places = new List<Place>
            {
                new Place
                {
                    Name = "Aroma",
                    Waiters  = new List<User> {context.Users.FirstOrDefault()},
                    Address = @"вулиця Академіка Гнатюка, 5/7, Львів, Львівська область, 79000",
                    GeoLat = 49.8326545,
                    GeoLong = 24.0107315,
                },
                new Place
                {
                    Name = "Sit/eat",
                    Waiters = new List<User> {context.Users.LastOrDefault() },
                    Address = @"вулиця Київська, 7, Львів, Львівська область, 79000",
                    GeoLat = 49.8412523,
                    GeoLong = 24.0261069,
                }
            };

            context.Places.AddRange(places);
            context.SaveChanges();

            var catogories = new List<ProductCategory>
            {
                new ProductCategory
                {
                    Name = "FirstCategory",
                    Place = context.Places.FirstOrDefault()
                },
                new ProductCategory
                {
                    Name = "SecondCategory",
                    Place = context.Places.LastOrDefault()
                },
            };

            context.ProductCategories.AddRange(catogories);
            context.SaveChanges();

            var products = new List<Product>
            {
                new Product
                {
                    Name = "Coffee",
                    Price = 30,
                    Description = "Great coffee for morning",
                    ProductCategory = context.ProductCategories.FirstOrDefault()

                },
                new Product
                {
                    Name = "Tea",
                    Price = 20,
                    Description = "Tea for evening near to fireplace",
                    ProductCategory = context.ProductCategories.FirstOrDefault()
                },
                new Product
                {
                    Name = "Pizza",
                    Price = 100,
                    Description = "Very spicy with chicken and banana",
                    ProductCategory = context.ProductCategories.LastOrDefault()
                }
            };

            context.AddRange(products);
            context.SaveChanges();
        }
    }
}
