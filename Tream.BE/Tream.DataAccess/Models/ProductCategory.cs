﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tream.DataAccess.Models
{
    public class ProductCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int PlaceId { get; set; }

        public virtual Place Place { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
