﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Tream.DataAccess.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OrderStatus
    {
        AddedToCart,
        Pending,
        AwaitingPayment,
        InProgress,
        Canceled,
        AwaitingForCustomer,
        Completed
    }

    public class Order
    {
        public int Id { get; set; }

        public virtual Place Place { get; set; }

        public virtual User User { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime CompletionDate { get; set; }

        public string Preferences { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
    }
}
