﻿using System.Collections;
using System.Collections.Generic;

namespace Tream.DataAccess.Models
{
    public class Place
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double GeoLong { get; set; } 

        public double GeoLat { get; set; }

        public string Address { get; set; }

        public virtual ICollection<User> Waiters { get; set; }

        public virtual ICollection<ProductCategory> ProductCategories { get; set; } 

    }
}
