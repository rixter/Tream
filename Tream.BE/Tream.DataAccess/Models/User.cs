﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Tream.DataAccess.Models
{
    public class User: IdentityUser
    {
        public int? JobId { get; set; }

        public virtual Place Job { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
