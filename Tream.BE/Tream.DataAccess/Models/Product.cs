﻿using System.Collections.Generic;

namespace Tream.DataAccess.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        //public string PictureUrl { get; set; }

        public virtual ProductCategory ProductCategory { get; set; }

        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
    }
}
