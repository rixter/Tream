﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.Web.CQRS.Queries;

namespace Tream.Web.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Places")]
    public class PlaceController : Controller
    {
        private readonly IQueryProcessorAsync processorAsync;

        public PlaceController(IQueryProcessorAsync processorAsync)
        {
            this.processorAsync = processorAsync;
        }

        /// <summary>
        /// Returns places the nearest to user
        /// </summary>
        /// <param name="lat">User latitude</param>
        /// <param name="log">User longitute</param>
        /// <param name="number">Number of places</param>
        [HttpGet("")]
        public async Task<IActionResult> GetNearestPlaces(double lat, double log, int number)
        {
            var query = new NearestPlacesQuery(lat, log, number);
            var result = await this.processorAsync.ProcessAsync(query);

            return Ok(result);
        }

        [HttpGet("{placeId}")]
        public async Task<IActionResult> GetPlaceData(int placeId)
        {
            var query = new PlaceDataQuery(placeId);
            var result = await this.processorAsync.ProcessAsync(query);

            return Ok(result);
        }

        [Authorize]
        [HttpGet("favourites")]
        public async Task<IActionResult> GetFavourites(int count = 10)
        {
            var userName = User.Identity.Name;
            var query = new FavouritePlacesQuery(userName, count);
            var result = await this.processorAsync.ProcessAsync(query);

            return Ok(result);
        }

    }
}