﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess.Models;
using Tream.Web.Configuration;
using Tream.Web.Contracts;
using Tream.Web.CQRS.Queries;
using Tream.Web.Helpers;
using Tream.Web.ViewModels;

namespace Tream.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IJwtFactory jwtFactory;
        private readonly IQueryProcessorAsync processorAsync;
        private readonly AuthOptions jwtOptions;

        public AccountController(UserManager<User> userManager, 
            IJwtFactory jwtFactory, 
            IOptions<AuthOptions> jwtOptions,
            IQueryProcessorAsync processorAsync)
        {
            this.userManager = userManager;
            this.jwtFactory = jwtFactory;
            this.processorAsync = processorAsync;
            this.jwtOptions = jwtOptions.Value;
        }

        [HttpGet("")] 
        [Authorize]
        public async Task<IActionResult> GetUser()
        {
            var userName = User.Identity.Name;
            var query = new UserByNameQuery(userName);
            var result = await this.processorAsync.ProcessAsync(query);

            return Ok(result);
                
        }

        /// <summary>
        /// Action for logging in with credentials
        /// </summary>
        [HttpPost("login")]
        public async Task<IActionResult> Post([FromBody]CredentialsViewModel credentials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var identity = await GetClaimsIdentity(credentials.UserName, credentials.Password);
            if (identity == null)
            {
                return BadRequest(Errors.AddErrorToModelState("login_failure", "Invalid username or password.", ModelState));
            }

            var jwt = await Tokens.GenerateJwt(identity, 
                this.jwtFactory, 
                credentials.UserName, 
                this.jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });

            return Ok(jwt);
        }

        /// <summary>
        ///     Action for register new user
        /// </summary>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<IActionResult> Post([FromBody]RegistrationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userIdentity = new User
            {
                UserName = model.Name,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber
            };

            var result = await this.userManager.CreateAsync(userIdentity, model.Password);

            if (!result.Succeeded) return new BadRequestObjectResult(Errors.AddErrorsToModelState(result, ModelState));

            return new OkObjectResult("Account created");
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                return await Task.FromResult<ClaimsIdentity>(null);
            }

            var userToVerify = await this.userManager.FindByNameAsync(userName);
            if (userToVerify == null)
            {
                return await Task.FromResult<ClaimsIdentity>(null);
            }

            if (await this.userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(this.jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }
    }
}