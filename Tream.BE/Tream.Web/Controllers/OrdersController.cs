﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Tream.Common.CQRS;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.Commands;
using Tream.Web.CQRS.DataTransferObjects;
using Tream.Web.CQRS.Queries;
using Tream.Web.Helpers;
using Tream.Web.ViewModels;

namespace Tream.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Orders")]
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly IQueryProcessorAsync queryProcessorAsync;

        private readonly ICommandProcessorAsync<CommandResult> commandProcessor;
        private readonly IHubContext<OrdersHub> hubContext;

        public OrdersController(IQueryProcessorAsync queryProcessorAsync, 
            ICommandProcessorAsync<CommandResult> commandProcessor,
            IHubContext<OrdersHub> hubContext)
        {
            this.queryProcessorAsync = queryProcessorAsync;
            this.commandProcessor = commandProcessor;
            this.hubContext = hubContext;
        }


        [HttpGet("{orderId:int}")]
        public async Task<IActionResult> GetOrder(int orderId)
        {
            var query = new OrderQuery(orderId);

            OrderDto orderDto = await queryProcessorAsync.ProcessAsync(query);
            if (orderDto == null)
            {
                return NotFound();
            }

            orderDto.UpdateTotalPrice();

            return Ok(orderDto);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUserOrders()
        {
            UserDTO userDto = await GetCurrentUserInfo();

            var query = new OrdersQuery
            {
                UserId = userDto.Id
            };

            ICollection<OrderDto> orderDtos = await queryProcessorAsync.ProcessAsync(query);

            orderDtos.UpdateTotalPrices();

            return Ok(orderDtos);
        }

        [HttpGet("{orderId:int}/price")]
        public async Task<IActionResult> GetOrderTotalPrice(int orderId)
        {
            var query = new OrderQuery(orderId);

            OrderDto orderDto = await queryProcessorAsync.ProcessAsync(query);
            if (orderDto == null)
            {
                return NotFound();
            }

            orderDto.UpdateTotalPrice();

            return Ok(orderDto.TotalPrice);
        }

        [HttpGet("cart/place/{placeId:int}")]
        public async Task<IActionResult> GetUserCartActiveOrder(int placeId)
        {
            UserDTO userDto = await GetCurrentUserInfo();

            var query = new CardActiveOrderQuery(placeId, userDto.Id);

            OrderDto orderDto = await queryProcessorAsync.ProcessAsync(query);

            orderDto.UpdateTotalPrice();

            return Ok(orderDto);
        }

        [HttpPost("cart/order/confirm")]
        public async Task<IActionResult> ConfirmCartOrder([FromBody] int orderId, int placeId)
        {
            CommandResult commandResult = await ChangeOrderStatusAsync(orderId, OrderStatus.Pending);
            if (!commandResult.Success)
            {
                return BadRequest(commandResult.ErrorMessage);
            }



            var query = new OrderQuery(orderId);
            var order = await queryProcessorAsync.ProcessAsync(query);

            await hubContext.Clients.Group(placeId.ToString()).SendAsync("newOrder", order);
            return Ok();
        }

        [HttpPost("cart/order/cancel")]
        public async Task<IActionResult> CancelCartOrder([FromBody] int orderId)
        {
            CommandResult commandResult = await ChangeOrderStatusAsync(orderId, OrderStatus.Canceled);
            if (!commandResult.Success)
            {
                return BadRequest(commandResult.ErrorMessage);
            }

            var order = await queryProcessorAsync.ProcessAsync(new OrderQuery(orderId));

            await hubContext.Clients.Group(order.PlaceId.ToString())
                .SendAsync("statusChanged", order, OrderStatus.Canceled);
            return Ok();
        }

        [HttpGet("place/{placeId:int}")]
        public async Task<IActionResult> GetAllPlaceOrders(int placeId)
        {
            var query = new OrdersQuery
            {
                PlaceId = placeId
            };

            ICollection<OrderDto> orderDtos = await queryProcessorAsync.ProcessAsync(query);

            orderDtos.UpdateTotalPrices();

            return Ok(orderDtos);
        }

        [HttpGet("place/{placeId:int}/my")]
        public async Task<IActionResult> GetMyOrdersForPlace(int placeId)
        {
            UserDTO user = await GetCurrentUserInfo();

            var query = new OrdersQuery
            {
                PlaceId = placeId,
                UserId = user.Id
            };

            ICollection<OrderDto> orderDtos = await queryProcessorAsync.ProcessAsync(query);

            orderDtos.UpdateTotalPrices();

            MyPlaceOrdersViewModel myPlaceOrdersModel = await GetMyOrderPlacesModelAsync(placeId, orderDtos);

            return Ok(myPlaceOrdersModel);
        }

        [HttpGet("places/my")]
        public async Task<IActionResult> GetMyOrdersForPlace()
        {
            UserDTO user = await GetCurrentUserInfo();

            var query = new OrdersQuery
            {
                UserId = user.Id
            };

            ICollection<OrderDto> orderDtos = await queryProcessorAsync.ProcessAsync(query);

            orderDtos.UpdateTotalPrices();

            IEnumerable<IGrouping<int, OrderDto>> groupByPlaces = orderDtos.GroupBy(dto => dto.PlaceId);

            var models = new List<MyPlaceOrdersViewModel>();

            foreach (IGrouping<int, OrderDto> groupByPlace in groupByPlaces)
            {
                int placeId = groupByPlace.Key;

                IList<OrderDto> orders = groupByPlace.Select(dto => dto).ToList();

                MyPlaceOrdersViewModel model = await GetMyOrderPlacesModelAsync(placeId, orders);

                models.Add(model);
            }

            return Ok(models);
        }

        [HttpPost("query")]
        public async Task<IActionResult> QueryOrders([FromBody] QueryOrdersViewModel queryOrdersViewModel)
        {
            var query = new OrdersQuery
            {
                PlaceId = queryOrdersViewModel.PlaceId,
                OrderStatus = queryOrdersViewModel.Status,
                UserId = queryOrdersViewModel.UserId
            };

            ICollection<OrderDto> orderDtos = await queryProcessorAsync.ProcessAsync(query);

            orderDtos.UpdateTotalPrices();

            return Ok(orderDtos);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateOrder([FromBody] CreateOrderViewModel createOrderViewModel)
        {
            // TODO: Create mapping

            var createOrderCommand = new CreateOrderCommand
            {
                PlaceId = createOrderViewModel.PlaceId,
                Preferences = createOrderViewModel.Preferences,
                RequestorId = createOrderViewModel.RequestorId
            };

            foreach (ProductOrderDto productOrderDto in createOrderViewModel.ProductOrders)
            {
                createOrderCommand.ProductOrders.Add(productOrderDto);
            }

            CommandResult commandResult = await commandProcessor.ProcessAsync(createOrderCommand);
            if (!commandResult.Success)
            {
                return BadRequest();
            }

            return Ok("Order was created.");
        }

        [HttpPost("product/{productId:int}")]
        public async Task<IActionResult> AddProductToCart(int productId, int amount)
        {
            UserDTO user = await GetCurrentUserInfo();

            var addToCardCommand = new AddToCardCommand(productId, amount, user.Id);

            CommandResult commandResult = await commandProcessor.ProcessAsync(addToCardCommand);
            if (!commandResult.Success)
            {
                return BadRequest(commandResult.ErrorMessage);
            }

            return Ok();
        }

        [HttpPut("waiter/start")]
        public async Task<IActionResult> StartProcess([FromBody]StartOrderProgressCommand command)
        {
            var user = await GetCurrentUserInfo();
            command.UserId = user.Id;
            var result = await commandProcessor.ProcessAsync(command);
            if (!result.Success)
            {
                return BadRequest(result.ErrorMessage);
            }

            var order = await queryProcessorAsync.ProcessAsync(new OrderQuery(command.OrderId));

            await hubContext.Clients.Group(order.PlaceId.ToString())
                .SendAsync("statusChanged", order, OrderStatus.InProgress);
            return Ok();
        }

        [HttpPut("waiter/complete")]
        public async Task<IActionResult> CompleteProcess([FromBody]ChangeOrderStatusCommand command)
        {
            var user = await GetCurrentUserInfo();
            command.UserId = user.Id;
            command.NewStatus = OrderStatus.AwaitingForCustomer;
            var result = await commandProcessor.ProcessAsync(command);
            if (!result.Success)
            {
                return BadRequest(result.ErrorMessage);
            }

            var order = await queryProcessorAsync.ProcessAsync(new OrderQuery(command.OrderId));

            await hubContext.Clients.Group(order.PlaceId.ToString())
                .SendAsync("statusChanged", order, OrderStatus.AwaitingForCustomer);
            return Ok();
        }

        [HttpDelete("product/{productId:int}")]
        public async Task<IActionResult> RemoveProductFromCart(int productId)
        {
            UserDTO user = await GetCurrentUserInfo();

            var removeProductCommand = new RemoveProductCommand(productId, user.Id);

            CommandResult commandResult = await commandProcessor.ProcessAsync(removeProductCommand);
            if (!commandResult.Success)
            {
                return BadRequest(commandResult.ErrorMessage);
            }

            return Ok();
        }

        private async Task<UserDTO> GetCurrentUserInfo()
        {
            string userName = User.Identity.Name;

            var query = new UserByNameQuery(userName);

            UserDTO currentUserInfo = await queryProcessorAsync.ProcessAsync(query);

            return currentUserInfo;
        }

        private async Task<CommandResult> ChangeOrderStatusAsync(int orderId, OrderStatus newStatus)
        {
            UserDTO user = await GetCurrentUserInfo();

            var command = new ChangeOrderStatusCommand(orderId, user.Id, newStatus);

            CommandResult commandResult = await commandProcessor.ProcessAsync(command);

            return commandResult;
        }

        private async Task<MyPlaceOrdersViewModel> GetMyOrderPlacesModelAsync(int placeId, ICollection<OrderDto> orders)
        {
            OrderDto activeOrder = orders.FirstOrDefault(dto => dto.OrderStatus == OrderStatus.AddedToCart);
            if (activeOrder != null)
            {
                orders.Remove(activeOrder);
            }

            PlaceDTO placeDto = await queryProcessorAsync.ProcessAsync(new PlaceDataQuery(placeId));
            
            var model = new MyPlaceOrdersViewModel
            {
                PlaceId = placeId,
                PlaceName = placeDto.Name,
                ActiveOrder = activeOrder
            };

            foreach (OrderDto orderDto in orders)
            {
                model.Orders.Add(orderDto);
            }

            return model;
        }
    }
}