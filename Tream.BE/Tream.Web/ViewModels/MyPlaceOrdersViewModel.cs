﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.ViewModels
{
    public class MyPlaceOrdersViewModel
    {
        public OrderDto ActiveOrder { get; set; }

        public ICollection<OrderDto> Orders { get; } = new List<OrderDto>();

        public int PlaceId { get; set; }

        public string PlaceName { get; set; }
    }
}
