﻿using System;
using System.Collections.Generic;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.ViewModels
{
    public class CreateOrderViewModel
    {
        public int PlaceId { get; set; }

        public string RequestorId { get; set; }

        public string Preferences { get; set; }

        public IList<ProductOrderDto> ProductOrders { get; } = new List<ProductOrderDto>();
    }
}
