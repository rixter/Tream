﻿using FluentValidation.Attributes;
using System.ComponentModel.DataAnnotations;
using Tream.Web.ViewModels.Validators;

namespace Tream.Web.ViewModels
{
    [Validator(typeof(CredentialsViewModelValidator))]
    public class CredentialsViewModel
    {
        /// <summary>
        ///     User name or login
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        ///     User password
        /// </summary>
        public string Password { get; set; }
    }
}
