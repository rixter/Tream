﻿using Tream.DataAccess.Models;

namespace Tream.Web.ViewModels
{
    public class QueryOrdersViewModel
    {
        public OrderStatus? Status { get; set; }

        public int? PlaceId { get; set; }

        public string UserId { get; set; }
    }
}
