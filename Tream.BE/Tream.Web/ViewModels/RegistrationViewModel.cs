﻿using FluentValidation.Attributes;
using Tream.Web.ViewModels.Validators;

namespace Tream.Web.ViewModels
{
    [Validator(typeof(RegistrationViewModelValidator))]
    public class RegistrationViewModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        public string PhoneNumber { get; set; }

        public string PasswordConfirmation { get; set; }
    }
}
