﻿using FluentValidation;

namespace Tream.Web.ViewModels.Validators
{
    public class RegistrationViewModelValidator: AbstractValidator<RegistrationViewModel>
    {
        public RegistrationViewModelValidator()
        {
            RuleFor(vm => vm.Password)
                .NotEmpty()
                .Equal(vm => vm.PasswordConfirmation)
                .WithMessage("Password and password confirmation must be equals");

            RuleFor(vm => vm.Email)
                .NotEmpty()
                .EmailAddress()
                .WithMessage("Email not valid");

            RuleFor(vm => vm.Name)
                .NotEmpty()
                .WithMessage("Name is required");

            RuleFor(vm => vm.PhoneNumber)
                .NotEmpty()
                .WithMessage("Phone number is required");
        }
    }
}
