﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.DataTransferObjects;
using Tream.Web.CQRS.Queries;
using Tream.Web.Helpers;

namespace Tream.Web
{
    [Authorize]
    public class OrdersHub: Hub
    {
        private readonly TreamDBContext context;
        private readonly IQueryProcessorAsync queryProcessor;

        public OrdersHub(TreamDBContext context, IQueryProcessorAsync queryProcessor)
        {
            this.context = context;
            this.queryProcessor = queryProcessor;
        }
        public override async Task OnConnectedAsync()
        {
            var user = context.Users.Include(x => x.Job).FirstOrDefault(x => x.UserName == Context.User.Identity.Name);
            if (user.JobId != null)
            {
                await Groups.AddAsync(Context.ConnectionId, user.JobId.ToString());
                await Clients.Client(Context.ConnectionId).SendAsync("allOrders", GetPlaceOrders(user.JobId.Value));
            }
        }

        private async Task<ICollection<OrderDto>> GetPlaceOrders(int placeId)
        {
            var query = new OrdersQuery
            {
                PlaceId = placeId
            };

            var orderDtos = await queryProcessor.ProcessAsync(query);
            var orderDtosList = orderDtos.ToList();
            orderDtosList.RemoveAll(x => x.OrderStatus == OrderStatus.AddedToCart);
            orderDtosList.UpdateTotalPrices();
            return orderDtosList.OrderByDescending(x=> x.OrderDate).ToList();
        }
    }
}
