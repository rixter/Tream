﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.Helpers
{
    public static class OrdersHelper
    {
        public static double CalculateTotalPrice(this OrderDto order)
        {
            return order.ProductOrders.Sum(dto => dto.Product.Price * dto.Amount);
        }

        public static void UpdateTotalPrice(this OrderDto order)
        {
            double totalPrice = order.CalculateTotalPrice();
            order.TotalPrice = totalPrice;
        }

        public static void UpdateTotalPrices(this IEnumerable<OrderDto> orders)
        {
            foreach (OrderDto orderDto in orders)
            {
                orderDto.UpdateTotalPrice();
            }
        }
    }
}
