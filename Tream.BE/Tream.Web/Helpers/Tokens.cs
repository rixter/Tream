﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Tream.Web.Configuration;
using Tream.Web.Contracts;
using Tream.Web.Models;

namespace Tream.Web.Helpers
{
    public class Tokens
    {
        public static async Task<TokenResult> GenerateJwt(ClaimsIdentity identity, IJwtFactory jwtFactory, string userName, AuthOptions jwtOptions, JsonSerializerSettings serializerSettings)
        {
            var response = new TokenResult
            {
                Id = identity.Claims.Single(c => c.Type == "id").Value,
                AuthToken = await jwtFactory.GenerateEncodedToken(userName, identity),
                ExpiresIn = (int)jwtOptions.ValidFor.TotalSeconds
            };

            return response;
        }
    }
}
