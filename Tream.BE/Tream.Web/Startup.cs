﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;
using Tream.Common.CQRS;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.DataAccess.Models;
using Tream.Web.Configuration;
using Tream.Web.Contracts;
using Tream.Web.CQRS;
using Tream.Web.Extentions;
using Tream.Web.Helpers;

namespace Tream.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        { 
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            MapperConfiguration.Configure();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddIdentity<User, IdentityRole>().AddEntityFrameworkStores<TreamDBContext>();
            services.AddDbContext<TreamDBContext>(options => options.UseSqlServer(this.Configuration.GetSection("ConnectionStrings")["DefaultConnection"]));
            services.AddTransient<IJwtFactory, JwtFactory>();
            services.AddTransient<IQueryProcessorAsync, TreamQueryProcessor>();
            services.AddTransient<ICommandProcessorAsync<CommandResult>, TreamCommandProcessor>();

            services.AddMvc();
            services.AddAuthorizaion(this.Configuration);

            services.AddSignalR();

            var provider = services.BuildServiceProvider();

            var context = provider.GetService<TreamDBContext>();
            //comment this line before applling migration
            //DbInitializer.Initialize(context);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Tream API", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseAuthentication();
            app.UseMvc();

            app.UseSignalR(routes =>
            {
                routes.MapHub<OrdersHub>("/hub/orders");
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Tream API V1");
            });
        }
    }
}
