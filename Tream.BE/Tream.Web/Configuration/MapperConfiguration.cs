﻿using AutoMapper;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.Configuration
{
    public static class MapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<Place, PlaceDTO>()
                    .ForMember(m => m.Id, p => p.MapFrom(m => m.Id))
                    .ForMember(m => m.GeoLat, p => p.MapFrom(m => m.GeoLat))
                    .ForMember(m => m.GeoLong, p => p.MapFrom(m => m.GeoLong))
                    .ForMember(m => m.Name, p => p.MapFrom(m => m.Name))
                    .ForMember(m => m.Address, p => p.MapFrom(m => m.Address))
                    .ForMember(m => m.Categories, p => p.MapFrom(m => m.ProductCategories));

                config.CreateMap<User, UserDTO>()
                    .ForMember(m => m.Id, p => p.MapFrom(m => m.Id))
                    .ForMember(m => m.Name, p => p.MapFrom(m => m.UserName))
                    .ForMember(m => m.Email, p => p.MapFrom(m => m.Email))
                    .ForMember(m => m.EmailConfirmed, p => p.MapFrom(m => m.EmailConfirmed))
                    .ForMember(m => m.Phone, p => p.MapFrom(m => m.PhoneNumber))
                    .ForMember(m => m.JobId, p => p.MapFrom(m => m.JobId));

                config.CreateMap<ProductCategory, ProductCategoryDTO>()
                    .ForMember(m => m.Id, p => p.MapFrom(m => m.Id))
                    .ForMember(m => m.Name, p => p.MapFrom(m => m.Name))
                    .ForMember(m => m.Products, p => p.MapFrom(m => m.Products));

                config.CreateMap<Product, ProductDTO>()
                    .ForMember(m => m.Id, p => p.MapFrom(m => m.Id))
                    .ForMember(m => m.Name, p => p.MapFrom(m => m.Name))
                    .ForMember(m => m.Price, p => p.MapFrom(m => m.Price))
                    .ForMember(m => m.Description, p => p.MapFrom(m => m.Description))
                    .ForMember(m => m.CategoryId, p => p.MapFrom(m => m.ProductCategory.Id));
                    //.ForMember(m => m.PictureUrl, p => p.MapFrom(m => m.PictureUrl));
                    

                config.CreateMap<Order, OrderDto>()
                    .ForMember(m => m.Id, p => p.MapFrom(m => m.Id))
                    .ForMember(m => m.OrderDate, p => p.MapFrom(m => m.OrderDate))
                    .ForMember(m => m.CompletionDate, p => p.MapFrom(m => m.CompletionDate))
                    .ForMember(m => m.OrderStatus, p => p.MapFrom(m => m.OrderStatus))
                    .ForMember(m => m.PlaceId, p => p.MapFrom(m => m.Place.Id))
                    .ForMember(m => m.Preferences, p => p.MapFrom(m => m.Preferences))
                    .ForMember(m => m.RequestorId, p => p.MapFrom(m => m.User.Id))
                    .ForMember(m => m.RequestorName, p => p.MapFrom(m => m.User.UserName))
                    .ForMember(m => m.PlaceName, p => p.MapFrom(m => m.Place.Name))
                    .ForMember(m => m.ProductOrders, p => p.MapFrom(m => m.ProductOrders));

                config.CreateMap<ProductOrder, ProductOrderDto>()
                    .ForMember(m => m.Amount, p => p.MapFrom(m => m.Amount))
                    .ForMember(m => m.ProductId, p => p.MapFrom(m => m.Product.Id))
                    .ForMember(m => m.Product, p => p.MapFrom(m => m.Product));
            });
        }
    }
}
