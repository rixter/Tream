﻿namespace Tream.Web.Models
{
    public class TokenResult
    {
        public string Id { get; set; }

        public string AuthToken { get; set; }
        
        public int ExpiresIn { get; set; }
    }
}
