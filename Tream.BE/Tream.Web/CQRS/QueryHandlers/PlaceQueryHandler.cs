﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.Web.CQRS.DataTransferObjects;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.Queries;

namespace Tream.Web.CQRS.QueryHandlers
{
    public class PlaceQueryHandler :
        IQueryHandlerAsync<NearestPlacesQuery, ICollection<PlaceDTO>>,
        IQueryHandlerAsync<PlaceDataQuery, PlaceDTO>,
        IQueryHandlerAsync<FavouritePlacesQuery, ICollection<PlaceDTO>>
    {
        private readonly TreamDBContext context;

        public PlaceQueryHandler(TreamDBContext context)
        {
            this.context = context;
        }

        public async Task<ICollection<PlaceDTO>> Handle(NearestPlacesQuery query)
        {
            var places = await context.Places
                 .OrderBy(place => Math.Pow(place.GeoLat - query.Lat, 2) + Math.Pow(place.GeoLong - query.Log, 2))
                 .Take(query.Number)
                 .ToListAsync();

            var placesDTO = Mapper.Map<ICollection<Place>, ICollection<PlaceDTO>>(places);
            return placesDTO;
        }

        public async Task<PlaceDTO> Handle(PlaceDataQuery query)
        {
            PlaceDTO placeDTO = null;

            var place = await this.context.Places.FindAsync(query.PlaceId);
            if(place != null)
            {
                var productCategories = context.ProductCategories
                    .Include(pc => pc.Place)
                    .Include(pc => pc.Products)
                    .Where(pc => pc.Place.Id == query.PlaceId);

                place.ProductCategories = await productCategories.ToListAsync();
                placeDTO = Mapper.Map<Place, PlaceDTO>(place);
            }

            return placeDTO;
        }

        public async Task<ICollection<PlaceDTO>> Handle(FavouritePlacesQuery query)
        {
            var user = await this.context.Users.FirstOrDefaultAsync(u => u.UserName == query.UserName);
            var placeIds = this.context.ProductOrders
                .Include(po => po.Product)
                    .ThenInclude(po => po.ProductCategory)
                .Include(po => po.Order)
                    .ThenInclude(o => o.User)
                .Where(po => po.Order.User == user)
                .GroupBy(po => po.Product.ProductCategory.PlaceId)
                .OrderBy(g => g.Count())
                .Take(query.Count)
                .Select(g => g.Key);

            var places = await this.context.Places
                .Where(p => placeIds.Contains(p.Id))
                .ToListAsync();

            return Mapper.Map<ICollection<Place>, ICollection<PlaceDTO>>(places);
        }
    }
}
