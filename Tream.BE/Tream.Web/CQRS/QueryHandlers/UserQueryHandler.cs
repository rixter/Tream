﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.DataTransferObjects;
using Tream.Web.CQRS.Queries;

namespace Tream.Web.CQRS.QueryHandlers
{
    public class UserQueryHandler :
        IQueryHandlerAsync<UserByNameQuery, UserDTO>
    {
        private readonly TreamDBContext context;

        public UserQueryHandler(TreamDBContext context)
        {
            this.context = context;
        }
        public async Task<UserDTO> Handle(UserByNameQuery query)
        {
            var user = await this.context.Users
                .FirstOrDefaultAsync(u => u.UserName == query.UserName);

            UserDTO userDTO = null;
            if (user != null)
            {
                userDTO = Mapper.Map<User, UserDTO>(user);
            }
            return userDTO;
        }
    }
}
