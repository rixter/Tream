﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.DataTransferObjects;
using Tream.Web.CQRS.Queries;

namespace Tream.Web.CQRS.QueryHandlers
{
    public class OrdersQueryHandler : 
        IQueryHandlerAsync<OrderQuery, OrderDto>,
        IQueryHandlerAsync<OrdersQuery, ICollection<OrderDto>>,
        IQueryHandlerAsync<CardActiveOrderQuery, OrderDto>
    {
        private readonly TreamDBContext context;

        public OrdersQueryHandler(TreamDBContext context)
        {
            this.context = context;
        }

        public async Task<OrderDto> Handle(OrderQuery query)
        {
            Order order = await QueryOrders().FirstOrDefaultAsync(o => o.Id == query.OrderId);
            if (order == null)
            {
                return null;
            }

            OrderDto orderDto = Mapper.Map<Order, OrderDto>(order);
            return orderDto;
        }

        public async Task<ICollection<OrderDto>> Handle(OrdersQuery query)
        {
            IQueryable<Order> queryOrders = QueryOrders(query);

            ICollection<OrderDto> orderDtos = await GetCollectionResult(queryOrders);

            return orderDtos;
        }

        public async Task<OrderDto> Handle(CardActiveOrderQuery query)
        {
            var orderQuery = new OrdersQuery
            {
                PlaceId = query.PlaceId,
                OrderStatus = OrderStatus.AddedToCart,
                UserId = query.UserId
            };

            Order order = await QueryOrders(orderQuery).FirstOrDefaultAsync();
            if (order == null)
            {
                return null;
            }

            OrderDto orderDto = Mapper.Map<Order, OrderDto>(order);
            return orderDto;
        }

        private async Task<ICollection<OrderDto>> GetCollectionResult(IQueryable<Order> query)
        {
            IList<Order> orders = await query.ToListAsync();

            ICollection<OrderDto> orderDtos = Mapper.Map<ICollection<Order>, ICollection<OrderDto>>(orders);

            return orderDtos;
        }

        private IQueryable<Order> QueryOrders(OrdersQuery query)
        {
            IQueryable<Order> queryOrders = QueryOrders();

            if (query.PlaceId.HasValue)
            {
                queryOrders = queryOrders.Where(order => order.Place.Id == query.PlaceId.Value);
            }

            if (query.OrderStatus.HasValue)
            {
                queryOrders = queryOrders.Where(order => order.OrderStatus == query.OrderStatus.Value);
            }

            if (!String.IsNullOrEmpty(query.UserId))
            {
                queryOrders = queryOrders.Where(order => order.User.Id == query.UserId);
            }

            return queryOrders;
        }

        private IQueryable<Order> QueryOrders()
        {
            return context.Orders.Include(order => order.Place)
                .Include(order => order.User)
                .Include(order => order.ProductOrders)
                .ThenInclude(order => order.Product);
        }
    }
}
