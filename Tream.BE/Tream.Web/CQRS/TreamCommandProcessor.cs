﻿using Tream.Common.CQRS;
using Tream.DataAccess;
using Tream.Web.CQRS.CommandHandlers;
using Tream.Web.CQRS.Commands;

namespace Tream.Web.CQRS
{
    public class TreamCommandProcessor: CommandProcessorBase<CommandResult>
    {
        public TreamCommandProcessor(TreamDBContext context)
        {
            var createOrderCommandHandler = new CreateOrderCommandHandler(context);
            var removeProductCommandHandler = new RemoveProductCommandHandler(context);
            var changeOrderStatusCommandHanlder = new ChangeOrderStatusCommandHanlder(context);

            RegisterHandler<CreateOrderCommand, CreateOrderCommandHandler>(createOrderCommandHandler);
            RegisterHandler<AddToCardCommand, CreateOrderCommandHandler>(createOrderCommandHandler);
            RegisterHandler<ChangeOrderStatusCommand, ChangeOrderStatusCommandHanlder>(changeOrderStatusCommandHanlder);
            RegisterHandler<StartOrderProgressCommand, ChangeOrderStatusCommandHanlder>(changeOrderStatusCommandHanlder);
            RegisterHandler<RemoveProductCommand, RemoveProductCommandHandler>(removeProductCommandHandler);
        }
    }
}
