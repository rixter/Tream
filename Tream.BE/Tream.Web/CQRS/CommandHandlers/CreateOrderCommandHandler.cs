﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Storage;
using Tream.Common.CQRS;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.Commands;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.CQRS.CommandHandlers
{
    public class CreateOrderCommandHandler : 
        ICommandHandler<CreateOrderCommand, CommandResult>,
        ICommandHandler<AddToCardCommand, CommandResult>
    {
        private readonly TreamDBContext context;

        public CreateOrderCommandHandler(TreamDBContext context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(CreateOrderCommand command)
        {
            User user = await context.Users.FindAsync(command.RequestorId);
            if (user == null)
            {
                throw new InvalidOperationException($"Requestor with id = {command.RequestorId} was not found.");
            }

            Place place = await context.Places.FindAsync(command.PlaceId);
            if (place == null)
            {
                throw new InvalidOperationException($"Place with id = {command.PlaceId} was not found.");
            }

            var order = new Order
            {
                User = user,
                Place = place,
                Preferences = command.Preferences,
                OrderDate = DateTime.Now,
                OrderStatus = OrderStatus.Pending
            };

            IList<ProductOrder> productOrders = await CreateProductOrders(order, command.ProductOrders);
            
            int orderId = await MakeOrderAsync(order, productOrders);

            return new CommandResult(true);
        }

        private async Task<IList<Product>> GetProductsAsync(params int[] ids)
        {
            IList<Product> products = await context.Products
                .Where(product => ids.Contains(product.Id))
                .OrderBy(product => product.Id)
                .ToListAsync();

            return products;
        }

        private async Task<IList<ProductOrder>> CreateProductOrders(Order order, IList<ProductOrderDto> productOrderDtos)
        {
            IList<ProductOrder> productOrders = new List<ProductOrder>();

            if (productOrderDtos != null)
            {
                productOrderDtos = productOrderDtos.OrderBy(dto => dto.ProductId).ToList();

                int[] ids = productOrderDtos.Select(dto => dto.ProductId).ToArray();

                IList<Product> products = await GetProductsAsync(ids);

                for (int i = 0; i < products.Count; ++i)
                {
                    productOrders.Add(new ProductOrder
                    {
                        Order = order,
                        //Amount = productOrderDtos[i].Amount,
                        Product = products[i]
                    });
                }
            }

            return productOrders;
        }

        private async Task<int> MakeOrderAsync(Order order, IList<ProductOrder> productOrders)
        {
            int orderId;

            using (IDbContextTransaction transaction = context.Database.BeginTransaction())
            {
                try
                {
                    orderId = await SaveOrderAsync(order);
                    await UpdateOrderProductsAsync(order, productOrders);

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return orderId;
        }

        private async Task<int> SaveOrderAsync(Order order)
        {
            EntityEntry<Order> entityEntry = await context.Orders.AddAsync(order);

            await context.SaveChangesAsync();

            return entityEntry.Entity.Id;
        }

        private async Task UpdateOrderProductsAsync(Order order, IList<ProductOrder> productOrders)
        {
            await context.ProductOrders.AddRangeAsync(productOrders);

            await context.SaveChangesAsync();
        }

        public async Task<CommandResult> Handle(AddToCardCommand command)
        {
            Product product = context.Products
                .Include(p => p.ProductCategory)
                    .ThenInclude(c => c.Place)
                .FirstOrDefault(p => p.Id == command.ProductId);

            if (product == null)
            {
                return new CommandResult(false);
            }

            Order userActiveOrder = context.Orders
                .Include(order => order.ProductOrders)
                .Include(order => order.User)
                .Include(order => order.Place)
                .FirstOrDefault(order => order.User.Id == command.UserId 
                    && order.OrderStatus == OrderStatus.AddedToCart
                    && order.Place.Id == product.ProductCategory.PlaceId);

            if(userActiveOrder != null)
            {
                ProductOrder addedProduct =
                    userActiveOrder.ProductOrders.FirstOrDefault(productOrder =>
                        productOrder.ProductId == command.ProductId);

                if(addedProduct != null)
                {
                    addedProduct.Amount += command.Amount;
                }
                else
                {
                    context.ProductOrders.Add(new ProductOrder
                    {
                        Product = product,
                        Amount = command.Amount,
                        Order = userActiveOrder
                    });
                }
            }
            else
            {
                User user = context.Users.Find(command.UserId);

                context.Orders.Add(new Order
                {
                    Place = product.ProductCategory.Place,
                    OrderDate = DateTime.Now,
                    OrderStatus = OrderStatus.AddedToCart,
                    User = user,
                    ProductOrders = new List<ProductOrder>
                    {
                        new ProductOrder
                        {
                            Amount = command.Amount,
                            Product = product
                        }
                    }
                });
            }

            return new CommandResult(await context.SaveChangesAsync() > 0);
        }
    }
}
