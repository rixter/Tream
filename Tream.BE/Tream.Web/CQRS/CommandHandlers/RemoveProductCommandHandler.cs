﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tream.Common.CQRS;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.Commands;

namespace Tream.Web.CQRS.CommandHandlers
{
    public class RemoveProductCommandHandler : ICommandHandler<RemoveProductCommand, CommandResult>
    {
        private readonly TreamDBContext context;

        public RemoveProductCommandHandler(TreamDBContext context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(RemoveProductCommand command)
        {
            Product product = context.Products
                .Include(p => p.ProductCategory)
                .ThenInclude(c => c.Place)
                .FirstOrDefault(p => p.Id == command.ProductId);

            if (product == null)
            {
                return new CommandResult(false);
            }

            Order userActiveOrder = context.Orders
                .Include(order => order.ProductOrders)
                .Include(order => order.User)
                .Include(order => order.Place)
                .FirstOrDefault(order => order.User.Id == command.UserId
                                         && order.OrderStatus == OrderStatus.AddedToCart
                                         && order.Place.Id == product.ProductCategory.PlaceId &&
                                         order.ProductOrders.Any(productOrder =>
                                             productOrder.ProductId == command.ProductId));

            if (userActiveOrder == null)
            {
                return new CommandResult(false);
            }

            ProductOrder productOrderToRemove =
                userActiveOrder.ProductOrders.First(order => order.ProductId == command.ProductId);

            userActiveOrder.ProductOrders.Remove(productOrderToRemove);

            if (userActiveOrder.ProductOrders.Count == 0)
            {
                context.Remove(userActiveOrder);
            }

            return new CommandResult(await context.SaveChangesAsync() > 0);
        }
    }
}
