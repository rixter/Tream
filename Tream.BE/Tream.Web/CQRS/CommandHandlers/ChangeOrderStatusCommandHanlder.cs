﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tream.Common.CQRS;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.Commands;

namespace Tream.Web.CQRS.CommandHandlers
{
    public class ChangeOrderStatusCommandHanlder : 
        ICommandHandler<ChangeOrderStatusCommand, CommandResult>,
        ICommandHandler<StartOrderProgressCommand, CommandResult>
    {
        private readonly TreamDBContext context;

        public ChangeOrderStatusCommandHanlder(TreamDBContext context)
        {
            this.context = context;
        }

        public async Task<CommandResult> Handle(ChangeOrderStatusCommand command)
        {
            Order userOrder = await context.Orders.Include(x=> x.Place).Include(x => x.User).FirstOrDefaultAsync(x=> x.Id == command.OrderId);
            var user = await context.Users.FindAsync(command.UserId);
            if (userOrder == null || userOrder.User.Id != command.UserId && userOrder.Place.Id != user.JobId)
            {
                return new CommandResult(false);
            }

            userOrder.OrderStatus = command.NewStatus;

            return new CommandResult(await context.SaveChangesAsync() > 0);
        }

        public async Task<CommandResult> Handle(StartOrderProgressCommand command)
        {
            var user = await context.Users.FindAsync(command.UserId);
            Order userOrder = await context.Orders.Include(order => order.Place).FirstOrDefaultAsync(x => x.Id == command.OrderId);
            if (userOrder == null || userOrder.Place.Id != user.JobId)
            {
                return new CommandResult(false);
            }

            userOrder.OrderStatus = OrderStatus.InProgress;
            userOrder.CompletionDate = userOrder.OrderDate.AddMinutes(command.Time);
            return new CommandResult(await context.SaveChangesAsync() > 0);

        }
    }
}
