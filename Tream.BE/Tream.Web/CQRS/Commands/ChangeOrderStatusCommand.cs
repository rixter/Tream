﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess.Models;

namespace Tream.Web.CQRS.Commands
{
    public class ChangeOrderStatusCommand : ICommand
    {
        public ChangeOrderStatusCommand(int orderId, string userId, OrderStatus newStatus)
        {
            OrderId = orderId;
            UserId = userId;
            NewStatus = newStatus;
        }

        public int OrderId { get; set; }

        public string UserId { get; set; }

        public OrderStatus NewStatus { get; set; }
    }
}
