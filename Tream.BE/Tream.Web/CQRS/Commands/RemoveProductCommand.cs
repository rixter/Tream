﻿using Tream.Common.CQRS.Contracts;

namespace Tream.Web.CQRS.Commands
{
    public class RemoveProductCommand : ICommand
    {
        public RemoveProductCommand(int productId, string userId)
        {
            ProductId = productId;
            UserId = userId;
        }

        public int ProductId { get; }
        
        public string UserId { get; }
    }
}
