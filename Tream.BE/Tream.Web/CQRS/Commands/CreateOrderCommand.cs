﻿using System.Collections.Generic;
using Tream.Common.CQRS.Contracts;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.CQRS.Commands
{
    public class CreateOrderCommand : ICommand
    {
        public int PlaceId { get; set; }

        public string RequestorId { get; set; }

        public string Preferences { get; set; }

        public IList<ProductOrderDto> ProductOrders { get; } = new List<ProductOrderDto>();
    }
}
