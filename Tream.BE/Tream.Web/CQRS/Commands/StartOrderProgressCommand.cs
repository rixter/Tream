﻿using Tream.Common.CQRS.Contracts;

namespace Tream.Web.CQRS.Commands
{
    public class StartOrderProgressCommand: ICommand
    {
        public int OrderId { get; set; }

        public int Time { get; set; }

        public string UserId { get; set; }
    }
}
