﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tream.Common.CQRS.Contracts;

namespace Tream.Web.CQRS.Commands
{
    public class AddToCardCommand: ICommand
    {
        public AddToCardCommand(int productId, int amount, string userId)
        {
            ProductId = productId;
            Amount = amount;
            UserId = userId;
        }

        public int ProductId { get; }

        public int Amount { get; }

        public string UserId { get; }
    }
}
