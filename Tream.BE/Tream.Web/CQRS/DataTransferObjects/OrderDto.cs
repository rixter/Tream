﻿using System;
using System.Collections.Generic;
using Tream.DataAccess.Models;

namespace Tream.Web.CQRS.DataTransferObjects
{
    public class OrderDto
    {
        public int Id { get; set; }

        public int PlaceId { get; set; }

        public string RequestorId { get; set; }

        public string RequestorName { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime CompletionDate { get; set; }

        public string Preferences { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public double TotalPrice { get; set; }

        public string PlaceName { get; set; }

        public ICollection<ProductOrderDto> ProductOrders { get; set; }
    }
}
