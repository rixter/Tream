﻿namespace Tream.Web.CQRS.DataTransferObjects
{
    public class ProductOrderDto
    {
        public int Amount { get; set; }

        public int ProductId { get; set; }

        public ProductDTO Product { get; set; }
    }
}
