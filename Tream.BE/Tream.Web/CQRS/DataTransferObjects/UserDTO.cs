﻿namespace Tream.Web.CQRS.DataTransferObjects
{
    public class UserDTO
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string Phone { get; set; }

        public int JobId { get; set; }
    }
}
