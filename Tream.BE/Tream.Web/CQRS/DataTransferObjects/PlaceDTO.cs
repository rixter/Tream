﻿using System.Collections.Generic;

namespace Tream.Web.CQRS.DataTransferObjects
{
    public class PlaceDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double GeoLong { get; set; }

        public double GeoLat { get; set; }

        public string Address { get; set; }

        public ICollection<ProductCategoryDTO> Categories { get; set; }
    }
}
