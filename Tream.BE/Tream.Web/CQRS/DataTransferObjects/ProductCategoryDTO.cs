﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tream.Web.CQRS.DataTransferObjects
{
    public class ProductCategoryDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<ProductDTO> Products { get; set; }
    }
}
