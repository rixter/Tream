﻿using Tream.Common.CQRS.Contracts;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.CQRS.Queries
{
    public class PlaceDataQuery: IQuery<PlaceDTO>
    {
        public PlaceDataQuery(int placeId)
        {
            PlaceId = placeId;
        }

        public int PlaceId { get; }
    }
}
