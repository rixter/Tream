﻿using System.Collections.Generic;
using Tream.Common.CQRS.Contracts;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.CQRS.Queries
{
    public class FavouritePlacesQuery: IQuery<ICollection<PlaceDTO>>
    {
        public FavouritePlacesQuery(string userName, int count)
        {
            UserName = userName;
            Count = count;
        }

        public string UserName { get; }

        public int Count { get; }
    }
}
