﻿using System.Collections.Generic;
using Tream.Common.CQRS.Contracts;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.CQRS.Queries
{
    public class NearestPlacesQuery: IQuery<ICollection<PlaceDTO>>
    {
        public NearestPlacesQuery(double lat, double log, int number)
        {
            Lat = lat;
            Log = log;
            Number = number;
        }

        public double Lat { get; }

        public double Log { get; }

        public int Number { get; }
    }
}
