﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tream.Common.CQRS.Contracts;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.CQRS.Queries
{
    public class UserByNameQuery: IQuery<UserDTO>
    {
        public UserByNameQuery(string userName)
        {
            UserName = userName;
        }

        public string UserName { get; }
    }
}
