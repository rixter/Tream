﻿using System.Collections.Generic;
using Tream.Common.CQRS.Contracts;
using Tream.DataAccess.Models;
using Tream.Web.CQRS.DataTransferObjects;

namespace Tream.Web.CQRS.Queries
{
    public class OrderQuery : IQuery<OrderDto>
    {
        public OrderQuery(int orderId)
        {
            OrderId = orderId;
        }

        public int OrderId { get; }
    }

    public class OrdersQuery : IQuery<ICollection<OrderDto>>
    {
        public int? PlaceId { get; set; }

        public string UserId { get; set; }

        public OrderStatus? OrderStatus { get; set; }
    }

    public class CardActiveOrderQuery : IQuery<OrderDto>
    {
        public CardActiveOrderQuery(int placeId, string userId)
        {
            PlaceId = placeId;
            UserId = userId;
        }

        public int PlaceId { get; }

        public string UserId { get; }
    }
}
