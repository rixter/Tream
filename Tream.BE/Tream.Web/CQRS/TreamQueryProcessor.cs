﻿using System.Collections.Generic;
using Tream.Common.CQRS;
using Tream.DataAccess;
using Tream.Web.CQRS.DataTransferObjects;
using Tream.Web.CQRS.Queries;
using Tream.Web.CQRS.QueryHandlers;

namespace Tream.Web.CQRS
{
    public class TreamQueryProcessor: QueryProcessorBase
    {
        public TreamQueryProcessor(TreamDBContext context)
        {
            var placesQueryHander = new PlaceQueryHandler(context);
            var userQueryHandler = new UserQueryHandler(context);
            var orderQueryHandler = new OrdersQueryHandler(context);

            //TODO: Make query registarion automaticaly on runtime
            RegisterAsyncHandler<NearestPlacesQuery, ICollection<PlaceDTO>, PlaceQueryHandler>(placesQueryHander);
            RegisterAsyncHandler<FavouritePlacesQuery, ICollection<PlaceDTO>, PlaceQueryHandler>(placesQueryHander);
            RegisterAsyncHandler<UserByNameQuery, UserDTO, UserQueryHandler>(userQueryHandler);
            RegisterAsyncHandler<PlaceDataQuery, PlaceDTO, PlaceQueryHandler>(placesQueryHander);

            RegisterAsyncHandler<OrderQuery, OrderDto, OrdersQueryHandler>(orderQueryHandler);
            RegisterAsyncHandler<OrdersQuery, ICollection<OrderDto>, OrdersQueryHandler>(orderQueryHandler);
            RegisterAsyncHandler<CardActiveOrderQuery, OrderDto, OrdersQueryHandler>(orderQueryHandler);
        }
    }
}
