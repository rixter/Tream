import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SERVER_API } from 'api.constants';
import { Place } from 'models/place';
import * as _ from 'lodash';

@Injectable()
export class PlaceService {
    private placeUrl = `${SERVER_API}/api/Places`;

    constructor(private httpClient: HttpClient) { }

    public async getNearestPlaces(long: number, lat: number, number: number): Promise<Place[]> {
        const params = new HttpParams()
            .set('log', long.toString())
            .set('lat', lat.toString())
            .set('number', number.toString());

        const places = await this.httpClient.get<any>(`${this.placeUrl}/`, { params })
            .toPromise();

        return _.map(places, p => new Place(p));
    }

    public async getPlaceData(placeId: number) {
        const place = await this.httpClient.get(`${this.placeUrl}/${placeId}`)
            .toPromise();
        return new Place(place);
    }
}
