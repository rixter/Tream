import { MyPlaceOrders } from './../models/order';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_API } from 'api.constants';
import * as _ from 'lodash';
import { Order } from '../models/order';

@Injectable()
export class OrderService {
    private orderUrl = `${SERVER_API}api/Orders`;

    constructor(private httpClient: HttpClient) {}

    public async getUserOrders() {
        const orders = await this.httpClient.get<any>(`${this.orderUrl}`)
            .toPromise();

        return _.map(orders, order => new Order(order));
    }

    public async getUserOrder(orderId: number) {
        const order = await this.httpClient.get<any>(`${this.orderUrl}/${orderId}`)
            .toPromise();

        return new Order(order);
    }

    public async getPlaceOrders(placeId: number) {
        const orders = await this.httpClient.get<any>(`${this.orderUrl}/place/${placeId}`)
            .toPromise();

        return _.map(orders, order => new Order(order));
    }


    public async getUserOrdersByPlace(placeId: number) {
      const myOrdersModel = await this.httpClient.get(`${this.orderUrl}/place/${placeId}/my`)
            .toPromise();

      return new MyPlaceOrders(myOrdersModel);
    }

    public async getUserOrdersByPlaces() {
      const myOrdersModels = await this.httpClient.get<any>(`${this.orderUrl}/places/my`)
            .toPromise();

      return _.map(myOrdersModels, order => new MyPlaceOrders(order));
    }

    // public async getOrderTotalPrice(orderId: number) {
    //   const price = await this.httpClient.get(`${this.orderUrl}/${orderId}/price`)
    //         .toPromise();

    //   return price;
    // }

    public async confirmCartOrder(orderId: number, placeId: number) {
      await this.httpClient.post(`${this.orderUrl}/cart/order/confirm?placeId=${placeId}`, orderId, )
          .toPromise();
    }

    public async cancelCartOrder(orderId: number) {
      await this.httpClient.post(`${this.orderUrl}/cart/order/cancel`, orderId)
          .toPromise();
    }

    public async addProductToOrderCart(productId: number, amount: number) {
        await this.httpClient.post(`${this.orderUrl}/product/${productId}`, amount)
            .toPromise();
    }

    public async removeProductFromOrderCart(productId: number) {
      await this.httpClient.delete(`${this.orderUrl}/product/${productId}`)
          .toPromise();
    }

    public async startProgress(orderId, time) {
        await this.httpClient.put(`${this.orderUrl}/waiter/start`, { orderId, time })
            .toPromise();
    }

    public async completeProgress(orderId) {
        await this.httpClient.put(`${this.orderUrl}/waiter/complete`, { orderId })
        .toPromise();
    }
}
