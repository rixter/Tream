import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_API } from '../../api.constants';
import 'rxjs/add/operator/map';
import { UserLogin } from 'models/user-login.model';
import { UserRegister } from 'models/user-register.model';
import { TokenProvider } from 'services/auth/token.provider';

@Injectable()
export class AuthenticationService {
    private serverUrl = `${SERVER_API}/api/Account`;
    constructor(private http: HttpClient, private tokenProvider: TokenProvider) {}

    async login (user: UserLogin) {
        const response = await this.http
            .post<any>(`${this.serverUrl}/login`, user)
            .toPromise();

        console.log(response);
        this.tokenProvider.token = response.authToken;
    }

    async register (user: UserRegister) {
        await this.http.post(`${this.serverUrl}/register`, user)
            .toPromise();
    }

    async getUser() {
        const response = await this.http.get<any>(`${this.serverUrl}`)
            .toPromise();
            localStorage.setItem('jobId',response.jobId)
        return new UserRegister(response);
    }

    logout () {
        this.tokenProvider.removeToken();
    }
}
