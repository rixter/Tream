import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TokenProvider } from './token.provider';
import { SERVER_API } from '../../../api.constants';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private tokenProvider: TokenProvider) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url.indexOf(`${SERVER_API}`) === -1) {
          return next.handle(request);
        }
        else {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${this.tokenProvider.token}`
            }
          });
          return next.handle(request);
        }
      }
}
