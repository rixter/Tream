import { Injectable } from '@angular/core/';
import { TOKEN_NAME } from 'api.constants';

@Injectable()
export class TokenProvider {
	set token(value: string) {
        	localStorage.setItem(TOKEN_NAME, value);
	}

	get token(): string {
		return localStorage.getItem(TOKEN_NAME);
	}

	removeToken() {
		localStorage.removeItem(TOKEN_NAME);
	}
}
