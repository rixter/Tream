import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/interfaces';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TokenProvider } from './token.provider';

@Injectable()
export class AuthenticationGuard implements CanActivate {

    constructor(private router: Router, private tokenProvider: TokenProvider) {}

    canActivate(_route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (this.tokenProvider.token) {
			if (+localStorage.getItem('jobId')) {
				this.router.navigate([ '/orders' ], { queryParams: { returnUrl: state.url } });
			}
			return true;
		}

		// not logged in so redirect to login page with the return url
		this.router.navigate([ '/login' ], { queryParams: { returnUrl: state.url } });
		return false;
    }

	checkLogin(): Boolean {
		if (!this.tokenProvider.token) {
			return true;
		}
		return false;
	}
}
