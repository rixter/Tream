import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TokenProvider } from 'app/services';
import { SERVER_API, TOKEN_NAME } from 'api.constants';
import * as signalR from '@aspnet/signalr';
import { Order, OrderStatus } from '../models/order';

@Injectable()
export class OrderRealTimeService {

    private connection: signalR.HubConnection;
    constructor(private router: Router, private tokenProvider: TokenProvider) {
        this.connect();
    }

    private connect() {
        this.connection = new signalR.HubConnection(`${SERVER_API}hub/orders`,
            { accessTokenFactory: () => localStorage.getItem(TOKEN_NAME) }
        );
    }

    public startConnection(orderCreated: (oredr: Order) => void,
        orderStatusChanged: (order: Order, status: OrderStatus) => void,
        getAllOrders: (orders: Order[]) => void
    ) {
        if (!this.connection) {
            this.connect();
        }

        this.connection.start()
            .then(() => console.log('started'))
            .catch(error => {
            if (error.statusCode === 401) {
                this.tokenProvider.removeToken();
                this.router.navigate(['/login']);
            }
        });

        this.connection.on('newOrder', (orderData) => orderCreated(new Order(orderData)));
        this.connection.on('statusChanged', (order, status) => orderStatusChanged(order, status));
        // this.connection.on('orderUpdated', orderUpdated);
        this.connection.on('allOrders', (ordersData) => getAllOrders(ordersData.result));
    }

}
