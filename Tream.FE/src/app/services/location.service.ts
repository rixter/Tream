import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LocationService {
    private IPINFO_URL = "http://ipinfo.io";
    private JSONIP_URL = 'https://jsonip.com';
    constructor(private http: HttpClient) {}

    async getIp() {
        const data = await this.http.get<any>(`${this.JSONIP_URL}`)
        .toPromise();

        return data.ip;
    }

    async getLocationByIp(ipAddress: string) {
        const data = await this.http.get<any>(`${this.IPINFO_URL}/${ipAddress}`)
        .toPromise();

        return data.loc.split(',');
    }
}
