import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import * as material from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MAPS_API_KEY } from 'api.constants';
import * as components from './components';
import * as serivces from './services';
import { AppRoutingModule } from './app-routing.module';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { OrderRealTimeService } from 'services/order-realtime.service';
import { PlaceOrdersComponent } from 'app/components/place-orders/place-orders.component';

@NgModule({
  declarations: [
    AppComponent,
    components.MapComponent,
    components.HeaderComponent,
    components.LoginPageComponent,
    components.RegisterPageComponent,
    components.BackgroundBluredMapComponent,
    components.PlaceMenuComponent,
    components.FavouritePlacesComponent,
    components.UserMenuComponent,
    components.CartComponent,
    components.ProductComponent,
    PlaceOrdersComponent,
    components.OrderDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: MAPS_API_KEY
    }),
    material.MatButtonModule,
    material.MatCheckboxModule,
    material.MatToolbarModule,
    material.MatIconModule,
    material.MatCardModule,
    material.MatInputModule,
    material.MatFormFieldModule,
    material.MatExpansionModule,
    material.MatGridListModule,
    material.MatSidenavModule,
    material.MatListModule,
    material.MatDialogModule,
    material.MatTableModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    material.MatTabsModule,
    SimpleNotificationsModule.forRoot(),
  ],

  entryComponents: [
    components.CartComponent,
    components.OrderDetailsComponent
  ],
  providers: [
    serivces.PlaceService,
    serivces.AuthenticationService,
    serivces.AuthenticationGuard,
    serivces.OrderService,
    serivces.TokenProvider,
    serivces.SharedService,
    serivces.LocationService,
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {hasBackdrop: false}},
    OrderRealTimeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: serivces.HttpErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: serivces.TokenInterceptor,
      multi: true
    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
