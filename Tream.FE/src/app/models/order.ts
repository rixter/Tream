import { Product } from 'models/product';
import * as _ from 'lodash'

export class Order {
    public id: number;
    public placeId: number;
    public requestorId: string;
    public orderDate: Date;
    public completionDate: Date;
    public Preferences: string;
    public orderStatus: OrderStatus;
    public totalPrice: number;
    public placeName: string;
    public orderData: OrderData[];
    public isNew = false;
    public toEnd: Date;
    public mustBeCompleted = false;


    constructor(raw: any = {}) {
        _.assign(this, raw);
        this.orderData = _.map(raw.productOrders, x => new OrderData(x));
        this.totalPrice = _.sumBy(this.orderData, order => order.amount * order.product.price);
    }
}

export class OrderData {
    public amount: number;
    public product: Product;

    constructor(raw: any = {}) {
        this.amount = raw.amount;
        this.product = new Product(raw.product);
    }
}

export class MyPlaceOrders {
    public activeOrder: Order;
    public orders: Order[];
    public placeId: number;
    public placeName: string;

    constructor(raw: any = {}) {
      _.assign(this, raw);

      if (raw.activeOrder) {
        this.activeOrder = new Order(raw.activeOrder);
      }
      this.orders = _.map(raw.orders, x => new Order(x));
  }
}

export enum OrderStatus {
    AddedToCart = 'AddedToCart',
    Pending = 'Pending',
    AwaitingPayment = 'AwaitingPayment',
    InProgress = 'InProgress',
    Canceled = 'Canceled',
    AwaitingForCustomer = 'AwaitingForCustomer',
    Completed = 'Completed',
}
