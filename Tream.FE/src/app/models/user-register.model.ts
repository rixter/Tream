import * as _ from 'lodash';

export class UserRegister {
    email: string;
    password: string;
    name: string;
    location: string;
    phoneNumber: string;
    passwordConfirmation: string;
    jobId: number;

    constructor(raw: any = {}) {
        _.assign(this, raw);
    }
}