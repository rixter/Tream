export class Product {
    public id: number;
    public name: string;
    public description: string;
    public price: number;

    constructor(raw: any = {}) {
        this.id = raw.id;
        this.name = raw.name;
        this.description = raw.description;
        this.price = raw.price;
    }
}