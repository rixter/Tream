import { ProductCategory } from './product-category';
import * as _ from 'lodash';

export class Place {
    public id: number;
    public name: string;
    public geoLong: number;
    public geoLat: number;
    public address: string;
    public categories: ProductCategory[];

    constructor(raw: any = {}) {
        this.id = raw.id;
        this.name = raw.name;
        this.geoLong = raw.geoLong;
        this.geoLat = raw.geoLat;
        this.address = raw.address;
        this.categories = _.map(raw.categories, c => new ProductCategory(c));
    }
}
