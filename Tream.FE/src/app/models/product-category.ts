import { Product } from './product';
import * as _ from 'lodash';

export class ProductCategory {
    public id: number;
    public name: string;
    public products: Product[];

    constructor(raw: any = {}) {
        this.id = raw.id;
        this.name = raw.name;
        this.products = _.map(raw.products, p => new Product(p));
    }
}
