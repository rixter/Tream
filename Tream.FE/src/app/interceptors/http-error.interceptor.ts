import { Injectable } from '@angular/core';
import {
    HttpErrorResponse,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/empty';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
import { TokenProvider } from 'app/services/auth/token.provider';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private notify: NotificationsService, private router: Router, private tokenProvider: TokenProvider) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req)
        .catch((error: HttpErrorResponse) => {
            if (error.status === 401) {
                this.tokenProvider.removeToken();
                this.router.navigate(['/login']);
            }
            this.notify.error('Помилка обробки операції', error.status, {
                timeOut: 1000,
                showProgressBar: true,
                clickToClose: true
            });
            return Observable.empty<HttpEvent<any>>();
        });
    }
}
