import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Order, MyPlaceOrders } from '../../models/order';
import { OrderService } from 'services/order.service';
import { Product } from 'models/product';
import { NotificationsService } from 'angular2-notifications';
import _ = require('lodash');

@Component({
  selector: 'cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})

export class CartComponent implements OnInit {
    public orders: MyPlaceOrders[];

    constructor(public dialogRef: MatDialogRef<CartComponent>,
       private orderService: OrderService,
       private notify: NotificationsService) {
    }

    async ngOnInit() {
        this.orders = await this.orderService.getUserOrdersByPlaces();
    }

    async removeFromCart(placeOrdersModel : MyPlaceOrders, order: Order, product: Product) {
      await this.orderService.removeProductFromOrderCart(product.id);

      if(!this.removeOrderProduct(order, product)){
        this.orders = await this.orderService.getUserOrdersByPlaces();
      }
      else if(placeOrdersModel.activeOrder
        && placeOrdersModel.activeOrder.orderData.length === 0
        && !this.removeActiveOrder(placeOrdersModel)){
        this.orders = await this.orderService.getUserOrdersByPlaces();
      }
      else{
        await this.updateActiveOrderPrice(placeOrdersModel.placeId);
      }

      this.notify.info(`You removed ${product.name} from your cart`, 'See you later');
    }

    async changeAmount(placeId: number, product: Product, amount: number){
      await this.orderService.addProductToOrderCart(product.id, Math.round(amount));
      await this.updateActiveOrderPrice(placeId);
    }

    async confirmCartOrder(orderId: number, placeId: number) {
      await this.orderService.confirmCartOrder(orderId, placeId);
      this.orders = await this.orderService.getUserOrdersByPlaces();
    }

    async cancelCartOrder(orderId: number) {
      await this.orderService.cancelCartOrder(orderId);
      this.orders = await this.orderService.getUserOrdersByPlaces();
    }

    private async updateActiveOrderPrice(placeId: number) {
      const orderIndex = this.orders.findIndex(order => order.placeId === placeId);
      if (orderIndex === -1) {
        return;
      }

      const order = this.orders[orderIndex].activeOrder;
      if (!order) {
        return;
      }

      order.totalPrice = _.sumBy(order.orderData, orderData => orderData.amount * orderData.product.price);

      //  await this.orderService.getOrderTotalPrice(order.id).then(function(price: number) {
      //     order.totalPrice = price;
      //  });
    }

    private removeActiveOrder(placeOrdersModel: MyPlaceOrders): boolean {
      const orderIndexToRemove = this.orders.findIndex(o => o.activeOrder && o.activeOrder.id === placeOrdersModel.activeOrder.id);
      const canRemove: boolean = orderIndexToRemove !== -1;

      if (canRemove) {
        this.orders.splice(orderIndexToRemove, 1);
      }

      return canRemove;
    }

    private removeOrderProduct(order: Order, product: Product): boolean {
      const orderProductIndexToRemove = order.orderData.findIndex(orderData => orderData.product.id === product.id);
      const canRemove: boolean = orderProductIndexToRemove !== -1;

      if (canRemove) {
        order.orderData.splice(orderProductIndexToRemove, 1);
      }

      return canRemove;
    }
}

