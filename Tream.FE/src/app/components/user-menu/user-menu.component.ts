import { Component, OnInit } from '@angular/core';
import { Place } from 'models/place';
import { PlaceService } from 'app/services/place.service';
import { NotificationsService } from 'angular2-notifications';
import { AuthenticationService } from 'app/services/authentication.service';
import { UserRegister } from 'app/models/user-register.model';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from 'app/services/location.service';

@Component({
  selector: 'user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css']
})

export class UserMenuComponent implements OnInit {
    public nearestPlaces: Place[];
    public user: UserRegister = new UserRegister();
    public position: Position;
    public lattitude: number;
    public longitude: number;

    constructor(private placeService: PlaceService,
      private router: Router,
      private route: ActivatedRoute,
      private notificationService: NotificationsService,
      private authService: AuthenticationService,
      private locationService: LocationService) {}

    async ngOnInit() {
        this.user = await this.authService.getUser();

        navigator.geolocation.getCurrentPosition(async (pos) => {
            this.nearestPlaces = await this.placeService.getNearestPlaces(pos.coords.latitude, pos.coords.longitude, 10);
        },
        async (err) => {
            this.notificationService.alert('Get nearest places', 
            'Cannot get access to your position, so we get your nearest places based on approximate location');
            console.log(err);
            const ip = await this.locationService.getIp();
            const location = await this.locationService.getLocationByIp(ip);
            this.nearestPlaces = await this.placeService.getNearestPlaces(location[0], location[1], 10);
        }
        , { timeout: 2000 });

  }

    getPlaceData(place: Place) {
        console.log(place.id);
        const { router, route } = this;
        router.navigate(['/map/place', place.id], { relativeTo: route });
    }
}
