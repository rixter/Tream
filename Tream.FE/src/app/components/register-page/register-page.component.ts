import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserRegister } from 'models/user-register.model';
import { AuthenticationService } from 'services/authentication.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})

export class RegisterPageComponent {
    private email = new FormControl('', [Validators.required, Validators.email]);

    public hide = true;
    public user: UserRegister = new UserRegister();

    constructor (private authenticationService: AuthenticationService,
                 private notificationService: NotificationsService,
                 private router: Router) {}

    async register(user: UserRegister) {
        await this.authenticationService.register(user);
        await this.router.navigate(['/login']);
        this.notificationService.success('You successfully registered', 'Please, log in');
    }

    getEmailErrorMessage() {
        return this.email.hasError('required') ? 'You must enter your vaild e-mail' :
            this.email.hasError('email') ? 'Not a valid email' : '';
      }
}
