import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { OrderService } from 'app/services/order.service';
import { Order, OrderStatus } from 'app/models/order';

@Component({
  selector: 'order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})

export class OrderDetailsComponent {
    public time: number = 5;
    public orderStatus = OrderStatus;
    constructor(@Inject(MAT_DIALOG_DATA) public data: { order: Order }, private orderSerivce: OrderService) {
      console.log(data);
      debugger;
    }

    async startProgress() {
      await this.orderSerivce.startProgress(this.data.order.id, this.time);
    }

    async completeProgress() {
      await this.orderSerivce.completeProgress(this.data.order.id);
    }

    async cancelProgress() {
      await this.orderSerivce.cancelCartOrder(this.data.order.id);
    }

}

