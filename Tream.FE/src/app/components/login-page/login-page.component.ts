import { Component } from '@angular/core';
import { AuthenticationService } from 'services/authentication.service';
import { UserLogin } from 'models/user-login.model';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})

export class LoginPageComponent {
    hide = true;
    user: UserLogin = new UserLogin();
    constructor (private authenticationService: AuthenticationService,
                 private router: Router,
                 private notificationService: NotificationsService) {}

    async login(user: UserLogin) {
        await this.authenticationService.login(user);
        await this.router.navigate(['/map']);
        this.notificationService.success('You successfully log in', 'Welcome, username!');
    }

}
