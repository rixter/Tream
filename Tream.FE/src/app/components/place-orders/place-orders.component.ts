import { Component } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Order, OrderStatus } from 'app/models/order';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
// import { OrderService } from 'app/services';
import { OrderDetailsComponent } from 'app/components';
import { OrderRealTimeService } from 'app/services/order-realtime.service';
import * as _ from 'lodash';
// import { setInterval } from 'timers';
// import * as _ from 'lodash';

@Component({
    selector: 'place-orders',
    styleUrls: ['./place-orders.component.css'],
    templateUrl: './place-orders.component.html'
  })

  export class PlaceOrdersComponent implements OnInit {
    public orderStatus = OrderStatus;
    private orders: Order[];
    public displayedColumns = ['id', 'requestorId', 'totalPrice', 'timeToEnd', 'orderDate', 'status', 'details'];
    public dataSource;

    constructor(  public dialog: MatDialog,
                  private orderService: OrderRealTimeService  ) {}

    async ngOnInit() {
      this.orderService.startConnection((order) => {
        order.isNew = true;
        this.orders.unshift(order);
        this.dataSource = new MatTableDataSource(this.orders);
      },
      (order: Order, status: OrderStatus) => {
        const currentOrder = _.find(this.orders, x => x.id === order.id);
        currentOrder.orderStatus = status;
        this.dataSource = new MatTableDataSource(this.orders);
      },
        (o) => { this.orders = o;
        this.dataSource = new MatTableDataSource(this.orders);
        });
        setInterval(() => {
          _.each(this.orders, order => {
            const date =  new Date();
            const completionDate = new Date(order.completionDate);
            const dateStamp = (new Date()).getTime() - completionDate.getTime();
            date.setTime( Math.abs(dateStamp)  );
            order.toEnd = date;
            if (dateStamp < 0) {
              order.mustBeCompleted = true;
            }
            console.log(date);
          })
        } , 1000);
    }


    async openOrderDetails(order: Order) {
      order.isNew = false;
      this.dialog.open(OrderDetailsComponent, {
          width: '60%',
          data: {
            order
          }
      });
    }
}