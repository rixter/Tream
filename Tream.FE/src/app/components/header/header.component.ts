import { Component } from '@angular/core';
import { AuthenticationService } from 'services/authentication.service';
import { Router } from '@angular/router';
import { AuthenticationGuard } from 'services/auth/authentication.guard';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent {
    private isLogin: Boolean;

	constructor(
        private authGuard: AuthenticationGuard,
        private authService: AuthenticationService,
        private notificationService: NotificationsService,
        private router: Router
	) {
        router.events.subscribe(() => this.isLogin = this.authGuard.checkLogin());
	}

	logout() {
        this.authService.logout();
        this.router.navigate(['/login']);
        this.notificationService.info('You successfully log out', 'See you later');
        }
}
