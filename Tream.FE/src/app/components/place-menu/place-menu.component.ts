import { Component, OnInit } from '@angular/core';
import { Place } from 'models/place';
import { PlaceService } from 'app/services/place.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { SharedService } from 'services/shared.service';
import { Product } from 'models/product';
import { OrderService } from 'services/order.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
    selector: 'place-menu',
    styleUrls: ['./place-menu.component.css'],
    templateUrl: './place-menu.component.html'
  })

  export class PlaceMenuComponent implements OnInit {
    private place: Place;

    constructor(router: Router,
      private route: ActivatedRoute,
      private placeService: PlaceService,
      private orderService: OrderService,
      private sharedService: SharedService,
      private notify: NotificationsService) {
        router.events.subscribe(() => this.place = null);
    }

    async ngOnInit() {
      this.route.paramMap.subscribe(async (params: ParamMap) => {
          const placeId = +params.get('id');
          this.place = await this.placeService.getPlaceData(placeId);
          this.sharedService.emitChange(this.place);
      });

    }

    changeMapPositionToPlace(place) {
      console.log(place);
      this.sharedService.emitChange(place);
    }

    async addToCart(product: Product) {
      await this.orderService.addProductToOrderCart(product.id, 1);
      this.notify.info(`You added ${product.name} to your cart`, 'See you later');
    }
}