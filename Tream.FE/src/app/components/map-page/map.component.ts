import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { PlaceService } from 'services/place.service';
import { Place } from 'models/place';

import { ActivatedRoute, Router } from '@angular/router';

import { MatDialog } from '@angular/material';
import { CartComponent } from 'components/cart/cart.component';
import { SharedService } from 'app/services/shared.service';
import { LocationService } from 'app/services/location.service';

@Component({
  selector: 'map',
  styleUrls: ['./map.component.css'],
  templateUrl: './map.component.html'
})

export class MapComponent implements OnInit {
    public position: Position;
    public lattitude: number;
    public longitude: number;
    public userLattitude: number;
    public userLongitude: number;
    public centerLattitude: number;
    public centerLongitude: number;
    public places: Place[];
    public selectedPlace: Place;
    public isOpened = false;
    public iconUrl = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';

    constructor(private notification: NotificationsService,
        public dialog: MatDialog,
        private placeService: PlaceService,
        private router: Router,
        private route: ActivatedRoute,
        private sharedService: SharedService,
        private locationService: LocationService) {

        this.sharedService.changeEmitted$.subscribe(
            data => {
                this.lattitude = this.centerLattitude;
                this.longitude = this.centerLongitude;
                setTimeout((() => {
                    this.lattitude = data.geoLat;
                    this.longitude = data.geoLong;
                }), 100);
            });
        }

    async ngOnInit() {

        navigator.geolocation.getCurrentPosition(async (pos) => {
            console.log(pos);
            this.userLattitude = pos.coords.latitude;
            this.userLongitude = pos.coords.longitude;

            this.lattitude = pos.coords.latitude;
            this.longitude = pos.coords.longitude;
            this.places = await this.placeService.getNearestPlaces(pos.coords.latitude, pos.coords.longitude, 10);
        },
        async () => {
            this.notification.alert('Geolocation error',
            'We cannot get access to your position, so we get your approximate location'); 

            const ip = await this.locationService.getIp();
            const location = await this.locationService.getLocationByIp(ip);
            if (location) {
                this.userLattitude = +location[0];
                this.userLongitude = +location[1];

                this.lattitude = +location[0];
                this.longitude = +location[1];
                this.places = await this.placeService.getNearestPlaces(location[0], location[1], 10);
            }

        }
        , { timeout: 1000 });
    }

    async getPlaceData(place: Place) {
        const { router, route } = this;

        if (this.canChangePlace(place)) {
            this.isOpened = true;
            router.navigate(['./place', place.id], { relativeTo: route });
            this.selectedPlace = place;
            this.openMenuAfterChange();
        }
    }

    private canChangePlace(place: Place) {
        const { selectedPlace, isOpened } = this;
        return !selectedPlace || place.id !== selectedPlace.id || !isOpened;
    }

    centerChange(event) {
        this.centerLattitude = event.lat;
        this.centerLongitude = event.long;
    }

    openMenuAfterMap() {
        setTimeout(() => this.isOpened = true, 500);
    }

    openMenuAfterChange() {
        this.isOpened = false;
        setTimeout(() => {
            this.isOpened = true;
        }, 300);
    }

    routeToUserMenu() {
        this.router.navigate(['/map/user']);
        this.openMenuAfterChange();
    }

    haveJob() {
        return +localStorage.getItem('jobId');
    }


    async openCart() {
        const dialogRef = this.dialog.open(CartComponent, {
            data: { }
        });

        dialogRef.afterClosed().subscribe(_result => {
            // const orders = result;
        });
    }
}
