import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import * as component from './components';
import { PlaceOrdersComponent } from 'app/components/place-orders/place-orders.component';
const ROUTES: Routes = [
    {path: '', redirectTo: '/map/user', pathMatch: 'full'},
    {path: 'map', component: component.MapComponent,
    children: [
            { path: '', redirectTo: '/map/user', pathMatch: 'full' },
            { path: 'place/:id', component: component.PlaceMenuComponent },
            { path: 'main', component: component.FavouritePlacesComponent },
            { path: 'user', component: component.UserMenuComponent }
      ]},
      {path: 'orders', component: PlaceOrdersComponent},
    {path: 'login', component: component.LoginPageComponent},
    {path: 'register', component: component.RegisterPageComponent}
];

@NgModule({
    imports: [ RouterModule.forRoot(ROUTES) ],
    exports: [ RouterModule ]
  })
  export class AppRoutingModule {
  
  }